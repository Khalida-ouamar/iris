from typing import Any, Dict
import os
import pandas as pd
import numpy as np

import tensorflow as tf 
from tensorflow.keras import models, layers 
from tensorflow.keras.utils import to_categorical

import mlflow
from mlflow import log_metric, log_param, log_artifacts

def train_model(train_x: pd.DataFrame, train_y: pd.DataFrame, parameters: Dict[str, Any]) -> tf.keras.Model:
    
    
    epochs = parameters["epochs"]
    optimizer = parameters["optimizer"]
    
    #
    # Create the network 
    # 
    network = models.Sequential() 
    network.add(layers.Dense(512, activation='relu', input_shape=(4,)))
    network.add(layers.Dense(3, activation='softmax'))
                
    #
    # compile the Network 
    # 
    network.compile(optimizer=optimizer,
                    loss='categorical_crossentropy',
                    metrics=['accuracy'])
    
    train_labels = to_categorical(train_y)
    
    network.fit(train_x, train_labels, epochs=epochs, batch_size=40)
    return network
    

def evaluate(network: tf.keras.Model, test_x: pd.DataFrame, test_y: pd.DataFrame) -> np.ndarray: 
    test_labels = to_categorical(test_y)
    test_loss, test_acc = network.evaluate(test_x, test_labels)
    print("Test accuracy: ", test_acc, "\nTest loss: ", test_loss)
    return (test_acc, test_loss)
    
def predict(network: tf.keras.Model, test_x: pd.DataFrame)-> np.ndarray: 
    
    predictions = network.predict(test_x)
    return predictions


def report_accuracy(network: tf.keras.Model, test_acc: float, test_loss: float) -> None:
       
    mlflow.set_tracking_uri("http://localhost:5000")
    
    mlflow.set_experiment("iris")
    
    with mlflow.start_run():
        if not os.path.exists("outputs"):
            os.makedirs("outputs")
        with open("outputs/predictions.txt", "w") as f:
            f.write(f'Test accuracy {test_acc} \nTest loss {test_loss}')
            
        log_artifacts("outputs")
        log_metric("accuracy", test_acc)
        log_metric("loss", test_loss)  
        
        mlflow.keras.log_model(network, "model", registered_model_name="Iris")
        
