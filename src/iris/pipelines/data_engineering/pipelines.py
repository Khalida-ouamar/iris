from kedro.pipeline import Pipeline, node 

from .nodes import split_data


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                split_data,
                None,
                dict(
                    train_x="train_x",
                    train_y="train_y",
                    test_x="test_x",
                    test_y="test_y"
                ),
                name="split"
            )
         
         
        ]
    )